#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval,netflix_pred,netflix_check
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
import time

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

	#general use
	#jut check to see if the last element's rsme < 1
	def test_eval_gen(self):
		r = StringIO("10040:\n2417853\n1207062\n2487973\n")
		w = StringIO()
		netflix_eval(r, w)
		temp_str = w.getvalue().split("\n")
		self.assertLess(float(temp_str[-2]), 1)
			
	#users all gave same rating:4
	def test_eval_same(self):
		r = StringIO("10009:\n2263612\n701514\n10214:\n2370466\n512348\n")
		w = StringIO()
		netflix_eval(r,w)
		temp_str = w.getvalue().split("\n")
		self.assertLess(float(temp_str[-2]), 1)
		
	def test_eval_doubleMov(self):
		r = StringIO("8315:\n8316:\n275869\n2533823\n2264897\n10040:\n2417853\n1207062\n2487973\n")
		w = StringIO()
		netflix_eval(r,w)
		temp_str = w.getvalue().split("\n")
		self.assertLess(float(temp_str[-2]), 1)
	
	#try to get an idea of the time it takes to run over probe
	#len of probe = 1408395
	def test_eval_time(self):
		t0 = time.time()
		for i in range(1408395):
			r = StringIO("10025:\n1791064\n")
			w = StringIO()
			netflix_eval(r,w)
			temp_str = w.getvalue().split("\n")
		t1 = time.time()
		self.assertLess(t1-t0,60)
		
	# ----
	# pred
	# ----
	
	def test_pred_general(self):
		movie_id = 10018
		cust_id = 305767
		self.assertEqual(isinstance(netflix_pred(cust_id,movie_id),float),True)
		
	def test_pred_flip(self):
		movie_id = 305767
		cust_id = 10018
		self.assertEqual(netflix_pred(cust_id,movie_id),False)
	
	def test_pred_fail(self):
		movie_id = 2043
		cust_id = 0000000
		self.assertEqual(netflix_pred(cust_id,movie_id),False)
		
	# -----
	# check
	# -----
	
	def test_check_fail(self):
		movie_id = 2043
		cust_id = 716091
		self.assertEqual(netflix_check(cust_id,movie_id),False)
		
	def test_check_general(self):
		movie_id = 10018
		cust_id = 305767
		self.assertEqual(isinstance(netflix_check(cust_id,movie_id),int),True)
	
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
