#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, calculate_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_movie_parsing_1(self):
        r = StringIO("10040:\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n")

    def test_movie_parsing_2(self):
        r = StringIO("2043:\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "2043:\n")

    def test_movie_parsing_3(self):
        r = StringIO("2043:\n20145:\n46227:\n348:\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "2043:\n20145:\n46227:\n348:\n")

    def test_rmse_1(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n")
        w = StringIO()
        netflix_eval(r,w)
        rmse  = w.getvalue().split("\n")
        rmse = rmse[len(rmse) - 2]
        self.assertEqual(rmse,'0.55')

    def test_rmse_2(self):
        r = StringIO("10004:\n1737087\n84804\n735147\n")
        w = StringIO()
        netflix_eval(r,w)
        rmse = w.getvalue().split("\n")
        rmse = rmse[len(rmse) - 2]
        self.assertEqual(rmse,'0.71')

    def test_rmse_3(self):
        r = StringIO("10014:\n1626179\n839609\n430376\n1359761\n")
        w = StringIO()
        netflix_eval(r,w)
        rmse = w.getvalue().split("\n")
        rmse = rmse[len(rmse) - 2]
        self.assertLessEqual(rmse,'1.0')

    def test_calculateprediction_1(self):
        movie = 10316
        customer = 2018877
        prediction = calculate_prediction(movie,customer)
        prediction = float(str(prediction)[:3])
        self.assertEqual(prediction, 3.8)

    def test_calculateprediction_2(self):
        movie = 10331
        customer = 1330463
        prediction = calculate_prediction(movie,customer)
        prediction = float(str(prediction)[:3])
        self.assertEqual(prediction, 3.4)

    def test_calculateprediction_3(self):
        movie = 4354
        customer = 257830
        prediction = calculate_prediction(movie,customer)
        prediction = float(str(prediction)[:3])
        self.assertEqual(prediction, 3.1)

# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""

